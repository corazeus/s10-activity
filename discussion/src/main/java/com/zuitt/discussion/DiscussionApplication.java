package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@RestController
@SpringBootApplication
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}
/*

	//Retrieving all posts
	//http://localhost:8080/posts
//	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	@GetMapping("/posts")
	public String getPosts() {
		return "All posts retrieved";
	}

	//Creating New Post
	//http://localhost:8080/posts
//	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	@PostMapping("/posts")
	public String createPost(){
		return "New Post Created";
	}

	//Retrieving a single post
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.GET)
	@GetMapping("/posts/{postid}")
	public String getPosts(@PathVariable Long postid){
		return "Viewing details of post " +postid;
	}

	//Deleting a single Post
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
	@DeleteMapping("/posts/{postid}")
	public String deletePost(@PathVariable Long postid){
		return "Post " +postid+ " has been deleted.";
	}

	//Update a post
	//@RequestBodt anootation allows us to retrieve information sent via the request generated bt the frontend.
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
	@PutMapping("/posts/{postid}")
	@ResponseBody
	//@ResponseBody annotation allows the response to be sent back to the client in JSON format
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

	//Retrieving posts of a particular user
//	@RequestMapping(value = "/myposts", method = RequestMethod.GET)
	//@RequestHeader annotation allows us to grab information from the request header.
	@GetMapping("/myposts")
	public String getMyPosts(@RequestHeader(value = "Authorization") String user){
		return "Posts for " +user+ " have been retrieved.";
	}
*/

	//TODO: ACTIVITY STARTS HERE
	//Create a REST API for users
	//Includes: CRUD

	//New User
	@PostMapping("/users")
	public String newUser(){
		return "New user created";
	}

	//Get All Users
	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved";
	}

	//Delete User by ID
	@DeleteMapping("/users/{id}")
	public String deleteUser(@RequestHeader(value = "Authorization", required = false) String authorization, @PathVariable("id") String id){
		if(authorization.equals("") || authorization.equals(null)){
			return "Unauthorized access";
		}else {
			return "The user " +id+ " has been successfully deleted!";
		}
	}

	//Update user by ID
	@PutMapping("/users/{id}")
	@ResponseBody
	public String updateUser(@PathVariable(value = "id") String id, @RequestBody String name){
		return name;
	}

}
