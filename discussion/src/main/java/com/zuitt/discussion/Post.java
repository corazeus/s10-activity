package com.zuitt.discussion;

public class Post {

    //Declare Properties
    private String title;

    //Constructors
    public Post(){}
    public Post(String title) {
        this.title = title;
    }

    //Getter
    public String getTitle() {
        return title;
    }

    //Setter
    public void setTitle(String title) {
        this.title = title;
    }
}
